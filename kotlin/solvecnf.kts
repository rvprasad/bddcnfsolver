/*
 * Copyright (c) 2019 Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

@file:CompilerOpts("-jvm-target 1.8")
@file:DependsOn("com.github.ajalt:clikt:2.8.0")
@file:DependsOn("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.0")
@file:DependsOn("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
//@file:KotlinOpts("-J-ea")
@file:KotlinOpts("-J-Xmx4096M")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.lang.IllegalStateException
import java.util.concurrent.LinkedBlockingQueue
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.streams.toList
import kotlin.text.Regex

private typealias Assignment = Set<Int>
private typealias Clause = List<Int>
private typealias Var = UInt
private typealias Level = UInt

internal class CommandLine : CliktCommand(name = "kscript solvecnf.kts",
        printHelpOnEmptyArgs = true,
        help = "BDDNode based complete CNF solver") {
    private val cnf by option("-c", "--cnf", help = "CNF file in DIMACS format " +
            "(http://www.satcompetition.org/2009/format-benchmarks2009.html)").required()

    override fun run() {
        fun getLevelsForVars(clauses: Collection<Clause>, vars: Set<Var>) =
                vars.sortedByDescending { v ->
                    v.toInt().let { vLit -> clauses.count { vLit in it || -vLit in it } }
                }.mapIndexed { idx, v -> Pair(v, idx.toUInt()) }.toMap()

        val (clauses, vars) = readClauses(cnf)
        BDDCNFSolver.solve(clauses, getLevelsForVars(clauses, vars)).run {
            if (isEmpty()) {
                println("s UNSATISFIABLE")
            } else {
                println("s SATISFIABLE")
                println("v ${sortedBy(::abs).joinToString(" ")} 0")
            }
        }
    }

    private fun readClauses(fileName: String): Pair<Collection<Clause>, Set<Var>> {
        var expectedNumVars = 0
        var expectedNumClauses = 0
        val infoRegex = Regex("""^p cnf \d+ \d+.*""")
        val clauseRegex = Regex("""^\s*-?\d.*""")
        fun tokenize(line: String) = line.trim().split(' ').filter { it.isNotBlank() }

        val clauses = File(fileName).bufferedReader().lineSequence().map { line ->
            val tokens = tokenize(line)
            when {
                infoRegex.containsMatchIn(line) -> {
                    expectedNumVars = Integer.parseInt(tokens[2])
                    expectedNumClauses = Integer.parseInt(tokens[3])
                    null
                }
                clauseRegex.containsMatchIn(line) -> tokens.map { it.toInt() }.takeWhile { it != 0 }.toList()
                else -> null
            }
        }.filterNotNull().toList()

        val seenVars = clauses.flatten().map { it.absoluteValue.toUInt() }.toSet()
        println("c Variables ${seenVars.size} seen $expectedNumVars expected")
        println("c Clauses ${clauses.size} seen $expectedNumClauses expected")

        return Pair(clauses, seenVars)
    }
}

internal object BDDCNFSolver {
    private fun simplifyClausesBasedOnValues(clauses: Collection<Clause>, assignment: Assignment) =
            clauses.filterNot { c -> c.any { it in assignment } }
                    .map { c -> c.filterNot { -it in assignment } }

    private tailrec fun performBCP(clauses: Collection<Clause>, assignment: Assignment = emptySet()):
            Pair<Collection<Clause>, Assignment> {
        if (clauses.all { it.size > 1 })
            return Pair(clauses, assignment)

        val valsFromUnitClauses = clauses.filter { it.size == 1 }.map { it[0] }.toSet()
        val simplifiedClauses = simplifyClausesBasedOnValues(clauses, valsFromUnitClauses)
        val newAssignment = assignment + valsFromUnitClauses
        return when {
            simplifiedClauses.any { it.isEmpty() } || newAssignment.any { -it in newAssignment } ->
                Pair(emptySet(), emptySet())
            clauses.size != simplifiedClauses.size && simplifiedClauses.isNotEmpty() ->
                performBCP(simplifiedClauses, newAssignment)
            else -> Pair(simplifiedClauses, newAssignment)
        }
    }

    fun solve(givenClauses: Collection<Clause>, var2level: Map<Var, Level>): Assignment {
        fun reduceBDDsWithOp(bdds: Collection<BDDNode<Any>>, op: OP) =
                bdds.stream().reduce { bdd1, bdd2 -> op.apply(bdd1, bdd2) }.get()

        val (clauses, assignmentFromBCP) = performBCP(givenClauses.map { it.distinct() })
        if (clauses.any { it.isEmpty() })
            return emptySet()

        if (clauses.isEmpty())
            return assignmentFromBCP

        return clauses.parallelStream().map { clause ->
            reduceBDDsWithOp(clause.map { BDDNode.makeNode(it, var2level[it.absoluteValue.toUInt()]!!) }, OP.OR)
        }.toList().run {
            when {
                this.any { it === TerminalNode.FALSE_NODE } -> listOf(listOf(TerminalNode.FALSE_NODE))
                this.all { it === TerminalNode.TRUE_NODE } -> listOf(listOf(TerminalNode.TRUE_NODE))
                else -> this.filterIsInstance<VarNode>().groupBy { it.v }.values
            }
        }.run {
            this.parallelStream().map { reduceBDDsWithOp(it, OP.AND) }.toList()
        }.run {
            fun product(l: List<VarNode>) =
                    l.mapIndexed(::Pair).flatMap { (i, f) -> l.drop(i + 1).map { Pair(f, it) } }

            tailrec fun helper(givenBdds: List<BDDNode<Any>>): BDDNode<Any>? {
                return when {
                    givenBdds.all { it === TerminalNode.TRUE_NODE } -> TerminalNode.TRUE_NODE
                    givenBdds.any { it === TerminalNode.FALSE_NODE } -> TerminalNode.FALSE_NODE
                    else -> {
                        val bdds = givenBdds.filterIsInstance<VarNode>()
                        if (bdds.size == 1)
                            return bdds[0]

                        val (unused, pairs) = product(bdds).sortedByDescending {
                            (it.first.numTruths + it.second.numTruths).toInt()
                        }.fold(Pair(bdds.toSet(), emptyList<Pair<VarNode, VarNode>>())) { acc, p ->
                            val (unused, pairs) = acc
                            val (f, s) = p
                            if (f in unused && s in unused) Pair(unused - setOf(f, s), pairs + p) else acc
                        }
                        val queue = LinkedBlockingQueue<BDDNode<Any>>()
                        val job = GlobalScope.launch {
                            pairs.parallelStream().forEach { queue.add(OP.AND.apply(it.first, it.second)) }
                        }
                        val newBdds = (1..pairs.size).fold(emptyList<BDDNode<Any>>()) { acc, _ ->
                            if (acc.isNotEmpty() && acc.last() === TerminalNode.FALSE_NODE) {
                                job.cancel()
                                return acc.last()
                            } else acc + queue.take()
                        }
                        return helper(newBdds + unused)
                    }
                }

            }

            helper(this)
        }.run {
            when (this) {
                is TerminalNode ->
                    when (this) {
                        TerminalNode.TRUE_NODE ->
                            assignmentFromBCP + var2level.keys.map(UInt::toInt).filterNot {
                                it in assignmentFromBCP || -it in assignmentFromBCP
                            }
                        TerminalNode.FALSE_NODE -> emptySet()
                    }
                is VarNode -> {
                    val assignment = getTrueAssignment()
                    if (assignment.isNotEmpty()) assignment.sortedBy(::abs).toSet() + assignmentFromBCP
                    else emptySet()
                }
                else -> throw RuntimeException("How's this possible?")
            }
        }
    }
}

internal interface BDDNode<out T> {
    val v: T
    val numTruths: UInt

    fun getTrueAssignment(): Assignment {
        fun <T> helper(c: BDDNode<T>, assignment: Assignment): Assignment =
                when {
                    c is VarNode -> c.v.toInt().let {
                        when (val result = helper(c.hi, assignment + it)) {
                            emptySet<Int>() -> helper(c.lo, assignment + -it)
                            else -> result
                        }
                    }
                    c is TerminalNode && c == TerminalNode.TRUE_NODE -> assignment
                    else -> emptySet()
                }

        return helper(this, emptySet())
    }

    companion object {
        fun makeNode(a: Boolean) = if (a) TerminalNode.TRUE_NODE else TerminalNode.FALSE_NODE

        fun makeNode(lit: Int, level: Level) = VarNode(lit.absoluteValue.toUInt(),
                makeNode(lit < 0), makeNode(lit > 0), level)
    }
}

internal enum class TerminalNode : BDDNode<Boolean> {
    FALSE_NODE {
        override val v = false
        override val numTruths = 0u
        override fun toString() = "$v"
    },
    TRUE_NODE {
        override val v = true
        override val numTruths = 1u
        override fun toString() = "$v"
    }

}

internal class VarNode constructor(override val v: Var, val lo: BDDNode<Any>, val hi: BDDNode<Any>,
                                   val level: Level) : BDDNode<Var> {
    private val cachedHashCode = 31 * (31 * v.toInt() + lo.hashCode()) + hi.hashCode()

    override val numTruths = lo.numTruths + hi.numTruths

    override fun toString() = "($v $lo $hi)"
    override fun hashCode() = cachedHashCode
    override fun equals(other: Any?) =
            other === this || (other is VarNode && v == other.v && lo == other.lo && hi == other.hi)
}

internal enum class OP {
    AND {
        override val shortCircuitValue = TerminalNode.FALSE_NODE
    },
    OR {
        override val shortCircuitValue = TerminalNode.TRUE_NODE
    };

    abstract val shortCircuitValue: TerminalNode

    fun apply(b1: BDDNode<Any>, b2: BDDNode<Any>): BDDNode<Any> {
        val varNodes = HashMap<VarNode, VarNode>()

        fun makeNode(v: Var, lo: BDDNode<Any>, hi: BDDNode<Any>, level: Level) =
                if (lo === hi) hi
                else {
                    val tmp1 = VarNode(v, lo, hi, level)
                    varNodes.computeIfAbsent(tmp1) { tmp1 }
                }

        fun helper(a: BDDNode<Any>, b: BDDNode<Any>): BDDNode<Any> {
            return when (a) {
                is TerminalNode -> when (b) {
                    is TerminalNode -> when (this) {
                        AND -> BDDNode.makeNode(a.v && b.v)
                        OR -> BDDNode.makeNode(a.v || b.v)
                    }
                    is VarNode -> if (a === this.shortCircuitValue) this.shortCircuitValue else b
                    else -> throw IllegalStateException("How did this happen? $a $b")
                }
                is VarNode -> when (b) {
                    is TerminalNode -> if (b === this.shortCircuitValue) this.shortCircuitValue else a
                    is VarNode -> when {
                        a === b -> a
                        a.v == b.v -> {
                            val loBDD = helper(a.lo, b.lo)
                            val hiBDD = helper(a.hi, b.hi)
                            makeNode(a.v, loBDD, hiBDD, a.level)
                        }
                        else -> {
                            val (loLevelBDD, hiLevelBDD) = if (a.level < b.level) Pair(a, b) else Pair(b, a)
                            val loBDD = helper(loLevelBDD.lo, hiLevelBDD)
                            val hiBDD = helper(loLevelBDD.hi, hiLevelBDD)
                            makeNode(loLevelBDD.v, loBDD, hiBDD, loLevelBDD.level)
                        }
                    }
                    else -> throw IllegalStateException("How did this happen? $a $b")
                }
                else -> throw IllegalStateException("How did this happen? $a $b")
            }
        }

        return helper(b1, b2)
    }
}

CommandLine().main(args)
