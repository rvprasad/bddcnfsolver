# BDDCNFSolver

A tool to solve non-trivial CNF (i.e., with at least one clause) by converting them into BDDs. 

The tool provides information about the number of seen/expected variables and seen/expected clauses along with some info about the runtime behavior of the underlying algorithm.  The implementation of the tool in different languages is available in corresponding folder -- _Go_ and _Kotlin_.


## Attribution

Copyright (c) 2018, Venkatesh-Prasad Ranganath

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
